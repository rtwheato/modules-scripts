#!/bin/bash

## Setup kernel build env
KERNEL=kernel7

#First install Git and the build dependencies:
sudo apt-get -y install bc git bison flex libssl-dev libncurses5-dev dkms build-essential
sudo apt-get -y autoremove

# Next get the sources, and tools which will take some time:
echo "Getting sources and tools which will take some time..."
#Download sources fresh
rm -rf RaspberryPi_Beans; mkdir -p RaspberryPi_Beans; cd RaspberryPi_Beans
git clone https://github.com/raspberrypi/tools
git clone https://github.com/raspberrypi/linux -b rpi-4.11.y
git clone https://github.com/8devices/qcacld-2.0/ -b CNSS.LEA.NRT_3.0
# Add toolchain to PATH

export PATH=$PATH:$PWD/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin

#Prepare Linux

cd linux
make -j$(nproc) ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- bcm2709_defconfig

# Enable nl80211 test mode:

echo "Using the prompts, enable: Networking support -> Wireless -> nl80211 testmode command"
echo "Arrow keys navigate.  Enter selects.  Press esc twice to go back. Exiting out of the top level will ask you to save a .config"
# quick pause till user is ready
read -rsp $'Press any key to continue...\n' -n1 key

make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- menuconfig
#Save and exit

echo "Patching kernel..."
for i in ../qcacld-2.0/patches/kernel/v4.11/0* ; do patch -p1 < $i ; done

echo "Building kernel..."
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- zImage modules dtbs -j$(nproc)

echo "Patching qcacld..."
cd ../qcacld-2.0/
patch -p1 < patches/RaspberryPi/0001-destructor_rename.patch

echo "Building qcacld..."
KERNEL_SRC=../linux/ ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- make -j$(nproc)

echo "Building and installing the kernel, modules, and Device Tree blobs"
cd ../linux 
sudo cp /boot/$KERNEL.img /boot/$KERNEL-backup.img
sudo cp arch/arm/boot/dts/*.dtb /boot/
sudo cp arch/arm/boot/dts/overlays/*.dtb* /boot/overlays/
sudo cp arch/arm/boot/dts/overlays/README /boot/overlays/
sudo cp arch/arm/boot/zImage /boot/$KERNEL.img

echo "Changing /boot/config.txt"
echo core_freq=250 | sudo tee --append /boot/config.txt
echo dtoverlay=sdio,poll_once=false | sudo tee --append /boot/config.txt

echo "Installing modules..."
sudo make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- INSTALL_MOD_PATH=/ modules_install

cd ../qcacld-2.0/
sudo cp *.ko /home/pi/
sudo cp -r firmware_bin/usb/* /lib/firmware/
#sudo cp -r firmware_bin/sdio/* /lib/firmware/

echo "ALL DONE.  Connect the device to the Raspberry and load module with command `sudo insmod wlan-usb.ko`"
# #or

# sudo insmod wlan-sdio.ko

echo "The interface should appear using: `ip l`"

#  pi@raspberrypi:~ $ ip l
#  1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
#  	link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
#  2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP mode DEFAULT group default qlen 1000
#  	link/ether b8:27:eb:53:d0:9c brd ff:ff:ff:ff:ff:ff
#  3: wlan0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc pfifo_fast state DOWN mode DORMANT group default qlen 1000
#  	link/ether b8:27:eb:06:85:c9 brd ff:ff:ff:ff:ff:ff
#  8: wlan1: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN mode DORMANT group default qlen 3000
#  	link/ether c4:93:00:0f:7e:58 brd ff:ff:ff:ff:ff:ff
#  9: p2p0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN mode DORMANT group default qlen 3000
#  	link/ether c6:93:00:90:7e:58 brd ff:ff:ff:ff:ff:ff